terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-sydney-tfstate"
    key            = "recipe-app.tfstate"
    region         = "ap-southeast-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-sydney-tfstate-lock"
  }
}

provider "aws" {
  region  = "ap-southeast-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
