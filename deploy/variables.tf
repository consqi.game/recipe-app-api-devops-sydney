variable "prefix" {
  default = "raad" ## recipe-app-api-devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "iqbal@iqbal.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "867706719953.dkr.ecr.ap-southeast-2.amazonaws.com/recipe-app-api-devops-sydney:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "867706719953.dkr.ecr.ap-southeast-2.amazonaws.com/recipe-app-api-proxy-sydney:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

# variable "dns_zone_name" {
#   description = "Domain name"
#   default     = "cnsq.net"
# }

# variable "subdomain" {
#   description = "Subdomain per environment"
#   type        = map(string)
#   default = {
#     production = "api"
#     staging    = "api.staging"
#     dev        = "api.dev"
#   }
# }
